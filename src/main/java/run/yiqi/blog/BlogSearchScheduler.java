package run.yiqi.blog;

import javax.transaction.SystemException;
import org.hibernate.service.spi.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;
import run.yiqi.blog.dao.BlogDao;
import run.yiqi.blog.dao.es.BlogEsDao;
import run.yiqi.blog.model.Blog;
import run.yiqi.blog.model.BlogEs;


@Slf4j
@Component
public class BlogSearchScheduler {
    @Lazy @Autowired private BlogDao blogDao;
    @Lazy @Autowired private BlogEsDao blogEsDao;
    @Value("${x.es}") private boolean es = false;
    
    
    /**
     * 每天3点执行一次
     * @throws SystemException
     * @throws ServiceException
     */
    @Scheduled(cron="0 14 0 * * ?")
    public void synchronizeToEs() throws SystemException, ServiceException {
        if(es==false) return;
        log.info("synchronizeToEs >>>>>>>>>>>>>> ");
        
        Iterable<Blog> blogs = this.blogDao.findAll();
        blogs.forEach(blog->{
            BlogEs blogEs = new BlogEs();
            blogEs.setId(blog.getId());
            blogEs.setTitle(blog.getTitle());
            blogEs.setContent(blog.getContent());
            
            this.blogEsDao.save(blogEs);
        });
        
    }
}

package run.yiqi.blog.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class TagTypeVo implements Serializable {
    private static final long serialVersionUID = -2339738162035271674L;

    private String typeid;
    private String typename;
    private List<TagOptionVo> options = new ArrayList<>();

    private int sortOrder;
}

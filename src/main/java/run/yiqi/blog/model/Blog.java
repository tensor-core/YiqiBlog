package run.yiqi.blog.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import lombok.Getter;
import lombok.Setter;

@Entity
@Setter
@Getter
@EntityListeners(AuditingEntityListener.class)
//似乎放在最后不会报错, 放在前面会报错, 原因不明
//@Document(indexName="yiqiblog", type="blog")
public class Blog implements Serializable {
    private static final long serialVersionUID = 6961037288345418312L;

    //ID,自动生成唯一字符串
    @Id
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @GeneratedValue(generator = "uuid")
    private String id;

    //问题标题
    @Column(nullable = false)
    private String title;

    //问题描述
    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Basic(fetch = FetchType.LAZY)
    @Column(nullable = false)
    private String content;

    
        
    //主动方
    //问题对应的标签
    @ManyToMany(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    @JoinTable(name = "blog_tag_relation", joinColumns = @JoinColumn(name = "blog_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "tag_id", referencedColumnName = "optionid"))
    private Set<TagOption> tags = new HashSet<TagOption>();

    @Column(nullable = false)
    private Boolean star = false;

    //创建时间(注意: 问题发现时间 不等于 问题创建时间)
    @CreatedDate
    @Column(nullable = true)
    private LocalDateTime createDate;

    //更新时间
    @LastModifiedDate
    @Column(nullable = true)
    private LocalDateTime updateDate;

    //创建人(注意: 问题发现人 不等于 问题创建人)
    @CreatedBy
    private String createBy;

    //更新人
    @LastModifiedBy
    private String modifyBy;

}

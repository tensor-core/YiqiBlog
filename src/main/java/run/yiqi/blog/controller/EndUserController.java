package run.yiqi.blog.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import run.yiqi.blog.dao.EndUserDao;
import run.yiqi.blog.model.EndUser;
import run.yiqi.blog.model.Role;
import run.yiqi.blog.vo.EndUserVo;
import run.yiqi.blog.vo.RoleVo;
import run.yiqi.util.StringUtil;
import run.yiqi.util.Util;

@Slf4j
@Controller
public class EndUserController {
	
	@Lazy @Autowired
	private EndUserDao endUserDao;
	
	@ResponseBody
	@RequestMapping(value="/api/admin/enduser/query/listEndUsers", method=RequestMethod.GET)  
	public List<EndUserVo> listEndUsers() {
	    log.info("listEndUsers");
		List<EndUserVo> result = new ArrayList<EndUserVo>();
		
		Iterable<EndUser> eus = this.endUserDao.findAll(new Sort(Direction.ASC, "id"));
		
		eus.forEach(eu->{
		    EndUserVo euv = new EndUserVo();
            euv.setId(eu.getId());
            euv.setUsername(eu.getUsername());
            eu.getRoles().forEach(r->{
                RoleVo rv = new RoleVo();
                rv.setId(r.getId());
                rv.setName(r.getRoleName());
                euv.getRoles().add(rv);
            });
            result.add(euv);
		});
		return result;
	}

	
	@SneakyThrows
	@ResponseBody
	@RequestMapping(value="/api/admin/enduser/query/getMyInfo", method=RequestMethod.GET)  
	public EndUserVo getMyInfo() {
	    log.info("getMyInfo");
		EndUserVo result = new EndUserVo();
		
		EndUser eu = this.endUserDao.findByUsername(Util.getCurrentUsername());
		
		result.setId(eu.getId());
		result.setUsername(eu.getUsername());
		result.setPassword(eu.getPassword());
		
		eu.getRoles().forEach(r->{
            RoleVo rv = new RoleVo();
            rv.setId(r.getId());
            rv.setName(r.getRoleName());
            
            result.getRoles().add(rv);
		});
		return result;
	}
	
	
	@ResponseBody
	@RequestMapping(value="/api/admin/enduser/admin/saveUserInfo", method=RequestMethod.POST)  
	public void saveUserInfo(@RequestBody EndUserVo userVo ) {
	    log.info("saveUserInfo");

	       
		EndUser _eu = this.endUserDao.findById(userVo.getId()).get();
		_eu.getRoles().clear();
 
		for(int i = 0; i< userVo.getRoleIds().length; i++) {
			Role r = new Role();
			r.setId(userVo.getRoleIds()[i]);
			_eu.getRoles().add(r);
		}
		

		if(!StringUtil.isEmpty(userVo.getPassword())) {
		    String newPass = new BCryptPasswordEncoder().encode(userVo.getPassword());
			_eu.setPassword(newPass);
			
			
		}
		_eu = this.endUserDao.saveAndFlush(_eu);
		return;
	}
	
	

}
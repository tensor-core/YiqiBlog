package run.yiqi.blog.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import run.yiqi.blog.model.EndUser;


public interface EndUserDao extends JpaSpecificationExecutor<EndUser>, JpaRepository<EndUser, Long> {
    /**
     * 这个地方可以是findByUsername, 也可以是getByUsername, 两者的区别是当没有查询到记录时, get跑出异常,
     * 而find返回null
     * 
     * @param username
     * @return
     */
    EndUser findByUsername(String username);
}

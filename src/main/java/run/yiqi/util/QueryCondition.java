package run.yiqi.util;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 查询条件
 * @author Administrator
 *
 * @param <T>
 */
@Setter
@Getter
@ToString
public class QueryCondition<T> {
    //Page
    private QueryPage page = new QueryPage();
    
    //Sort
    private QuerySort sort = new QuerySort();

    //Filter
    //key: the field name
    //value: the value collections
    //参数传递post请求, 一定不能用Map或者HashMap,否则参数无法正确传递
    private List<QueryFilter> filters = new ArrayList<QueryFilter>();
    
    //Data
    private T data;
}


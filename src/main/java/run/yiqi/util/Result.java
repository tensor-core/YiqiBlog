package run.yiqi.util;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Result {
    private boolean result;
    private String description;
}

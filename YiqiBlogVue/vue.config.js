/**Vue配置 */
const CompressionWebpackPlugin = require(`compression-webpack-plugin`);
const productionGzipExtensions = [`js`, `css`, `ico`];

module.exports = {
    /**类似于servlet根路径, 需要通过http://localhoat:8080/vue/进行访问 */
    publicPath: `/vue/`,
    outputDir: `dist`,
    assetsDir: `assets`,
    productionSourceMap: false,
    filenameHashing: true,
    runtimeCompiler: false,
    integrity: false,

    /**开发调试服务器 */
    devServer: {
        host: `localhost`,
        port: 8080,
        proxy: `http://localhost:9090`,
        disableHostCheck: true
    },

    /**Webpack配置 */
    configureWebpack: {
        resolve: {
            alias: {
                vue$: `vue/dist/vue.js`
            }
        },
        plugins: [
            new CompressionWebpackPlugin({
                filename: `[path].gz[query]`,
                algorithm: `gzip`,
                test: new RegExp(`\\.(${productionGzipExtensions.join(`|`)})$`),
                threshold: 5120,
                minRatio: 0.8
            })
        ],
        externals: {
            /** 如果通过CDN的方式引入JS库, 则需要在这里列举出来 */
            /*'vue': `Vue`,
            'vue-router': `VueRouter`,
            'vuex': `Vuex`,
            'element-ui': `Element`,
            '@ckeditor/ckeditor5-build-decoupled-document': `DecoupledEditor`,
            'axios': `axios`,
            'vue-axios': `VueAxios`,
            'vue-i18n': `VueI18n`*/
        }
    },

    /**页面配置 */
    pages: {
        index: {
            entry: `src/main.js`,
            template: `public/index.html`,
            filename: `index.html`
        }
    },

    /**插件配置 */
    pluginOptions: {
        i18n: {
            locale: `zh`,
            fallbackLocale: `en`,
            localeDir: `locales`,
            enableInSFC: false
        }
    }
};